package examples;
import java.util.Scanner;
public class Switch {
public static void main( String[] args ) {
 System.out.print("From 1 to 2"); 
        Scanner scan = new Scanner(System.in); 
        int k = scan.nextInt(); 
        switch (k) { 
            case 1: { 
                System.out.print("One"); 
                break; 
            } 
 
            case 2:{ 
                System.out.print("Two"); 
                break; 
            } 
 
            default: { 
                System.out.print("None"); 
            } 
        
     }  
    }

}